Planetary Patrol

========================
Table of contents:
========================

	01.Description
	02.Project setup
	03.Goals & roadmap
	TODO

=======================
01.Description:
=======================

	2D side-scrolling shooterinspired by old MoonPatrol with elastomania-based physics and randomly generated terrain. Side functionality allows to use ini files to create different types of cars, with different handling. Two types are already provided - standard two wheeled Buggy ( well... four wheeled, but - you know - 2D and stuff? ) and 11 wheeled 'tank'.
	The objective of the game is to survive waves of enemies, jump or shoot random junk lying on your way (like mines or barrels) and jumping over craters.

=======================
02.Project setup:
=======================

	Before cloning, download & install Java JDK, Android SDK and Eclipse.
	For Eclipse, follow this libgdx tutorial how to setup Your envrionment: https://github.com/libgdx/libgdx/wiki/Setting-up-your-Development-Environment-%28Eclipse%2C-Intellij-IDEA%2C-NetBeans%29
	For HTML5 deployment: install GWT and Google Plugin in Your Eclipse (Google Plugin might still be incompatible with Luna. Use Keppler instead).

	Clone, and open project directory. Add new file 'local.properties' and add this line to it:

	sdk.dir=PATH/TO/YOUR/ANDROID/SDK

	Now import project into eclipse: import->gradle project.

=======================
03.Goals & roadmap
=======================

	TODO
