package com.elchupacabra.elchupapatrol.physics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.elchupacabra.elchupapatrol.Camera;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
 * 
 * @author Łukasz Piotrowski
 * 
 * Singleton class
 * 
 * Physics class is used to configure and control
 * box2D's instance of world, along with utility
 * methods for handling box2D fixtures.
 *
 */
public final class Physics {
	
	/** Physics World instance */
	public World world;
	/** settings instance */
	private Settings settings;
	/** debug renderer to draw box2d debug info on screen */
	private Box2DDebugRenderer debugRenderer;
	
	private static Physics instance = null;
	
	/**
	 * Creates new world and debug renderer
	 */
	private Physics() {
		settings = Settings.getInstance ();
		float[] gravity = { settings.getFloatValue( "gravityX" ),
							settings.getFloatValue( "gravityY" )	};
		world = new World( new Vector2( gravity[0], gravity[1]), true );
		debugRenderer = new Box2DDebugRenderer();
	}
	
	/**
	 * Returns instance of class or creates new
	 * if instance is null
	 * @return this class's instance
	 */
	public static Physics getInstance () {
		if( instance == null ){
			instance = new Physics();
			Gdx.app.log("Physics", "created Physics module");
		}
		return instance;
	}
	
	/**
	 * Allows for easy conversion of pixels to meters
	 * for coordinate pairs
	 * @param x coordinate in pixels
	 * @param y coordinate in pixels
	 * @return array with coordinates in meters
	 */
	public float[] getMetersFromPixels ( float x, float y ) {
		float modifier = settings.getFloatValue( "pixelToMeterModifier" );
		float[] meters = { x / modifier, y / modifier };
		return meters;
	}
	
	/**
	 * Allows for easy conversion of meters to pixels
	 * for coordinate pairs
	 * @param x coordinate in meters
	 * @param y coordinate in meters
	 * @return array with coordinates in pixels
	 */
	public float[] getPixelsFromMeters ( float x, float y ) {
		float modifier = settings.getFloatValue( "pixelToMeterModifier" );
		float[] pixels = { x * modifier, y * modifier };
		return pixels;
	}
	
	/**
	 * Steps physics world
	 * @todo add proper foolproof stepping system
	 */
	public void update () {
		world.step(	1/settings.getFloatValue( "fps" ), 
				settings.getIntValue( "positionIterations" ), 
				settings.getIntValue( "velocityIterations" )	);
	}
	
	/**
	 * Renders additional data for debbuging physics
	 */
	public void debugRender () {
		if( settings.getBoolValue( "debugDrawing" ) ){
			Matrix4 cameraCopy = Camera.getInstance().camera.combined.cpy();
			debugRenderer.render(world, cameraCopy.scl( settings.getFloatValue( "pixelToMeterModifier" ) ));
		}
	}
}
