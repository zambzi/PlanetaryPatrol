package com.elchupacabra.elchupapatrol.physics;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.elchupacabra.elchupapatrol.entities.EntityUserData;

public class CollisionListener implements ContactListener {
	
	private void contactEvent ( Fixture fixtureA, Fixture fixtureB, boolean isBegining ){
		if( fixtureA.getUserData() instanceof EntityUserData ){
			setCollidedTypes( (EntityUserData) fixtureA.getUserData(), (EntityUserData) fixtureA.getUserData() );
			if( isBegining ){
				eventOnBeginContact( fixtureA, fixtureB, 
					(EntityUserData) fixtureA.getUserData() );
			}
			else{
				eventOnEndContact( fixtureA, fixtureB, 
					(EntityUserData) fixtureA.getUserData() );
			}
		}
	}
	
	private void setCollidedTypes( EntityUserData dataA, EntityUserData dataB ){
		dataA.collidedType = dataB.type;
		dataB.collidedType = dataA.type;
	}

	@Override
	public void beginContact(Contact contact) {
		contactEvent( contact.getFixtureA(), contact.getFixtureB(), true );
		contactEvent( contact.getFixtureB(), contact.getFixtureA(), true );
	}
	
	@Override
	public void endContact(Contact contact) {
		contactEvent( contact.getFixtureA(), contact.getFixtureB(), false );
		contactEvent( contact.getFixtureB(), contact.getFixtureA(), false );
	}
		
	private void eventOnBeginContact( Fixture fixture, Fixture otherFixture, EntityUserData data ) {		
		switch( data.type ){
			case WHEEL : {
				data.isNonColliding = false;
				break;
			} case CHASSIS : {
				break;
			} case BULLET : {
				data.bulletCollision = true; 
				break;
			} case ENEMY_FLYER : {
				break;
			} case MINE : {
				break;
			} case STONE : {
				break;
			} case GROUND_SLICE : {
				break;
			} default : {
				
			}
		}
	}
	
	private void eventOnEndContact( Fixture fixture, Fixture otherFixture, EntityUserData data ) {	
		switch( data.type ){
			case WHEEL : {
				data.isNonColliding = true;
				break;
			} case CHASSIS : {
				break;
			} case BULLET : {
				data.bulletCollision = false; 
				break;
			} case ENEMY_FLYER : {
				break;
			} case MINE : {
				break;
			} case STONE : {
				break;
			} case GROUND_SLICE : {
				break;
			} default : {
				
			}
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		
	}

}
