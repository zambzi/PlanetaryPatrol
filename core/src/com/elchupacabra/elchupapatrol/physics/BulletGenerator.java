package com.elchupacabra.elchupapatrol.physics;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.elchupacabra.elchupapatrol.pawns.Bullet;

public class BulletGenerator {
	
	public float bulletForce = 50;
	public float bulletSpread = 10;
	public float firingRate = 20;
	public int currentBulletNumber = 1;
	
	private boolean isChamberReady = true;
	private int timer = 0;
	
//	private ArrayList<Bullet> firedBullets;
	
	public BulletGenerator( float bulletForce, float bulletSpread, float firingRate ){
		this.bulletForce = bulletForce;
		this.firingRate = firingRate;
		this.bulletSpread = bulletSpread;
//		firedBullets = new ArrayList<Bullet>();
		setTimer();
	}
	
	public void generateBullet( float x, float y, Vector2 direction ){
		if( isChamberReady ){
			Bullet bullet = new Bullet( x, y, direction, this.bulletForce, "bullet"+String.valueOf(this.currentBulletNumber) );
			setTimer();
		}
	}
	
	private void setTimer(){
		isChamberReady = false;
		timer = (int)Math.floor( (double)( 1000.0f/this.firingRate ) );
		if( timer <= 2 ){
			timer = 2;
		}
	}
	
	public void update(){
		timer--;
		if( timer <= 0 ){
			isChamberReady = true;
		}
//		for( Bullet bullet : firedBullets ){
//			if( bullet.isOutOfSCreen() ){
//				bullet.destroy();
//				firedBullets.remove( firedBullets.indexOf( bullet ) );
//				bullet = null;
//			}
//		}
	}
}
