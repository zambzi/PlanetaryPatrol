package com.elchupacabra.elchupapatrol.ground.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.elchupacabra.elchupapatrol.AssetsManager;
import com.elchupacabra.elchupapatrol.exceptions.NoAssetWithGivenNameException;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
 * 
 * @author Łukasz Piotrowski
 *
 * Singleton class
 *
 * Generates random 2D ground with
 * ridges, hills and craters.	
 *
 */
public class GroundGenerator {
	/** current texture name used with slices generated */
	private String groundTextureName;
	/** list containing all ground slices */
	private List<GroundSlice> slices;
	/** instance of TerrainShapeGenerator for shape randomization */
	private TerrainShapeGenerator terrainShapeGenerator;
	/** Settings instance */
	private Settings settings;
	/** contains X value of last generated slice vertex 
	 * to allow for proper vertex alignment between slices */
	private float lastGeneratedVertex = 0;
	
	private static GroundGenerator instance = null;
	
	
	private GroundGenerator() {
		settings = Settings.getInstance();
		slices = new ArrayList<GroundSlice>();
		terrainShapeGenerator = new TerrainShapeGenerator();
		groundTextureName = settings.getStringValue( "groundTexture" );
	}
	
	/**
	 * Returns instance of class or creates new
	 * if instance is null
	 * @return this class's instance
	 */
	public static GroundGenerator getInstance() {
		if( instance == null ){
			instance = new GroundGenerator();
			Gdx.app.log( "GroundGenerator", "created Ground Generator module" );
		}
		return instance;
	}
	
	/**
	 * Calculates proper verts for GroundSlice and creates new slice
	 * @param offsetX offsets horizontal position of slice
	 * @param upperHeights array with two Y coordinates for upper verticies
	 * of slice
	 * @return created GroundSlice
	 */
	private GroundSlice createSlice ( float offsetX, float[] upperHeights ) {
		if( upperHeights.length < 2 )
			throw new IndexOutOfBoundsException();
		float[] verts = {
				// 	X:											Y:
				0 ,												upperHeights[0], 	//upper left
				settings.getFloatValue( "sliceWidth" ), 		upperHeights[1], 	//upper right
				settings.getFloatValue( "sliceWidth" ) - 1,		0,					//lower right
				-1,												0					//lower left
		};
		
		return new GroundSlice( offsetX, 0, verts, groundTextureName, getTextureOffset( groundTextureName ) );
	}
	
	/**
	 * Calculates X offset for ground slice texture
	 * @param textureName name of a texture for AssetManager
	 * @return offset value
	 */
	private int getTextureOffset ( String textureName ) {
		int offset = 0;
		try {
			GroundSlice lastSlice = slices.get( slices.size() - 1 );
			int textureWidth =  (int)AssetsManager.getInstance().getTexture( groundTextureName ).getRegionWidth();
			if( lastSlice.textureOffsetX + settings.getFloatValue( "sliceWidth" )*2 > textureWidth ){
				offset = 0;
			} else {
				offset = (int)(lastSlice.textureOffsetX + settings.getFloatValue( "sliceWidth" ));
			}
			return offset;
		} catch ( IndexOutOfBoundsException ex ) {
			return 0;
		} catch ( NoAssetWithGivenNameException ex ){
			ex.getMessage();
			Gdx.app.exit();
			return 0;
		}
	}

	/**
	 * Takes care of process of generating GroundSlices, adding 
	 * them to list and applying proper terrain shape modification 
	 * to them.
	 * 
	 */
	private void generateGround() {
		if( slices.size() < settings.getFloatValue( "maxSlices" ) ){
			try{
				float lastItemOffset = 0;
				float[] heights = { settings.getFloatValue( "sliceHeight" ),
						settings.getFloatValue( "sliceHeight" ) };
				if( slices.size() > 0 ){
					GroundSlice lastSlice = slices.get( slices.size() - 1 );
					lastItemOffset = lastSlice
						.getPosition().x + settings.getFloatValue( "sliceWidth" );
					heights[0] = lastGeneratedVertex;
					heights[1] = terrainShapeGenerator.modifyTerrainVertex(
							settings.getFloatValue( "sliceHeight" ) );
				}
				slices.add( createSlice( lastItemOffset, heights ) );
				lastGeneratedVertex = heights[1];
			} catch ( Exception ex ){
				Gdx.app.error( "Ground Generator createSlice Exception",
						ex.getLocalizedMessage() );
			}
		} 
	}
	
	/**
	 * Destroys slice and removes it from list when
	 * it's out of screen
	 * @param slice slice to destroy
	 * @param i slice list iterator
	 */
	private void destroySliceOutOfScreen( GroundSlice slice, Iterator<GroundSlice> i) {
		if( slice.getPosition().x < -settings.getFloatValue( "sliceWidth" )  &&
				slices.size() >= settings.getFloatValue( "maxSlices" ) ){
			slice.destroy();
			i.remove();
		}
	}
	
	/**
	 * Takes care of entire process of moving, creating and
	 * destroying ground slices
	 */
	public void update(){
		generateGround();
		terrainShapeGenerator.update();
		Iterator<GroundSlice> i = slices.iterator();
		while (i.hasNext()) {
		   GroundSlice slice = i.next();
		   slice.moveSlice( settings.getFloatValue( "movementBaseSpeed" ) *
				   settings.getFloatValue( "difficulty" ) );
		   destroySliceOutOfScreen( slice, i );
		   slice.update();
		}
	}
	
	/**
	 * Renders slices via  PolygonSpriteBatch
	 * @param polyBatch batch to render ons
	 */
	public void render( PolygonSpriteBatch polyBatch ){
		for( GroundSlice slice : slices ){
			slice.render(polyBatch);
		}
	}
}
