package com.elchupacabra.elchupapatrol.ground.generator;

import java.util.Random;

import com.elchupacabra.elchupapatrol.utils.Settings;


/**
 * 
 * @author Łukasz Piotrowski
 * 
 * Small helper class for crater generation.
 * Times lengths (updated per ground slice generation)
 * of crater safe zones and craters themselves.
 *
 */
public class CraterTimer {
	public int timer;
	public int timerLimit;
	public boolean isBuildingCrater;
	private Random random;
	
	public CraterTimer( ) {
		isBuildingCrater = false;
		random = new Random( System.currentTimeMillis() );
		Settings settings = Settings.getInstance();
		setTimeLimit( 	settings.getIntValue( "beginZoneMin" ),
						settings.getIntValue( "beginZoneMax" ) );
	}
	
	private void setTimeLimit ( int min, int max ) {
		timer = 0;
		timerLimit = random.nextInt( min ) + ( max - min );
	}
	
	public boolean isItTimeToBuildCrater() {
		Settings settings = Settings.getInstance();
		timer++;
		if( timer > timerLimit ){
			if( isBuildingCrater ){
				setTimeLimit( settings.getIntValue( "safeZoneMin" ),
					(int)( settings.getIntValue( "safeZoneMax" ) * 
					1/settings.getIntValue( "difficulty" ) ) );
				return isBuildingCrater = false;
			} else {
				setTimeLimit( settings.getIntValue( "craterMin" ),
						settings.getIntValue( "craterMax" ) );
				return isBuildingCrater = true;
			}
		} else {
			if( isBuildingCrater ){
				return true;
			} else {
				return false;
			}
		}
	}
}
