package com.elchupacabra.elchupapatrol;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.elchupacabra.elchupapatrol.exceptions.NoAssetWithGivenNameException;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
 * @author Łukasz Piotrowski
 *
 *	Singleton Class
 *
 *	AssetsManager does just that - manages assets.
 *	Assets can be loaded to three buffers (music, texture,
 *	sound), and can be accessed via instance by any other class.
 */
public final class AssetsManager {
	/** Texture Buffer that holds all loaded textures as gdx Texture objects */
	private HashMap<String, AtlasRegion> textureBuffer;
	/** Sound Buffer that holds all loaded sounds as gdx Sound objects */
	private HashMap<String, Sound> soundBuffer;
	/** Music buffer that holds all loaded music as gdx Music objects  */
	private HashMap<String, Music> musicBuffer;
	
	private static AssetsManager instance = null;
	
	private AssetsManager () {
		textureBuffer = new HashMap<String, AtlasRegion>();
		soundBuffer = new HashMap<String, Sound>();
		musicBuffer = new HashMap<String, Music>();
		this.loadAssets();
	}
	
	/**
	*	Loads all selected assets to appropriate buffers
	*	@todo modify this method to be more flexible, maybe some kind of
	*	dynamic load of contents of assets folder of given quality on game start?
	*/
	private void loadAssets () {
		loadCarTextures();
		loadLevelTextures();
		Texture tex1 = new Texture(Gdx.files.internal("noise2.jpg"));
		TextureAtlas sheet = new TextureAtlas("sprites/misc/bullets");
		loadFromSheet( sheet );
		textureBuffer.put("ground", new AtlasRegion(tex1, 0, 0, tex1.getWidth(), tex1.getHeight() ) );
	}
	
	public void loadCarTextures (){
		String selectedCarAtlas = Settings.getInstance().getStringCarValue( "textureAtlas" );
		
		TextureAtlas sheet = new TextureAtlas("sprites/cars/" 
				+ selectedCarAtlas + "/" + selectedCarAtlas + ".txt");
		loadFromSheet( sheet );
	}
	
	public void loadLevelTextures(){
		//TODO: add level-selection
		TextureAtlas sheet = new TextureAtlas("sprites/levels/test");
		loadFromSheet( sheet );
	}
	
	private void loadFromSheet( TextureAtlas sheet ){
		for( AtlasRegion region : sheet.getRegions() ){
			textureBuffer.put( region.name, region );
			Gdx.app.log("textureName", region.name);
		}
	}
	
	/**
	*	Allows to get texture from buffer by name
	*	@return Texture object reference
	*   @throws NoAssetWithGivenNameException 
	*	@todo add exception handling
	*/
	public AtlasRegion getTexture ( String name ) throws NoAssetWithGivenNameException {
		AtlasRegion region = textureBuffer.get( name );
		if( region == null )
			throw new NoAssetWithGivenNameException( name, "texture" );
		return region;
	}
	
	/**
	*	Allows to get sound from buffer by name
	*	@return Sound object reference
	*/
	public Sound getSound ( String name ) {
		return soundBuffer.get( name );
	}
	
	/**
	*	Allows to get music from buffer by name
	*	@return Music object reference
	*/
	public Music getMusic ( String name ) {
		return musicBuffer.get( name );
	}
	
	/**
	*	Returns instance of given class, or creates new
	*	if that instance is nonexistent
	*	@return Class object instance
	*/
	public static AssetsManager getInstance() {
		if( instance == null ){
			instance = new AssetsManager();
			Gdx.app.log("AssetsManager", "created AssetsManager module");
		}
		return instance;
	}
}
