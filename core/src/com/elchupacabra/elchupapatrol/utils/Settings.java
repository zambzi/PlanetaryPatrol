package com.elchupacabra.elchupapatrol.utils;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.elchupacabra.elchupapatrol.exceptions.NoSuchCarInSettingsException;
import com.elchupacabra.elchupapatrol.exceptions.NoSuchKeyInSettingsException;

/**
*	@author Łukasz Piotrowski
*
*	Singleton Class
*
*	Settings class loads saved game configuration and
*	allows other classes to access it's data for setup.
*
*	ALL game parameters SHOULD be defined in config files
*	and called frome here via getValue or getCarValue, to avoid
*	hardcoding of values.
*
*
*/
public final class Settings {
	
	public HashMap<String, Object> baseSettings;
	public ArrayList<HashMap<String, Object>> carsSettings;
	public HashMap<String, Object> selectedCar;
	
	public int			DEFAULT_CAR_INDEX =			0;
	

	private static Settings instance = null;
	private ConfigFilesLoader configFilesLoader = null;
	
	private Settings() {
		baseSettings = new HashMap<String, Object>();
		carsSettings = new ArrayList<HashMap<String, Object>>();
	}
	
	private Object getBaseSettingsValue( String key ) throws NoSuchKeyInSettingsException {
		if( !baseSettings.containsKey( key ) ){
			throw new NoSuchKeyInSettingsException( key );
		}
		return baseSettings.get( key );
	}
	
	/** Gets saved setting value by key */
	public Object getValue( String key ) {
		Object value = null;
		try{
			value = getBaseSettingsValue( key );
		} catch( NoSuchKeyInSettingsException ex ){
			Gdx.app.error( "Exeption while loading setting", ex.getMessage() );
			Gdx.app.exit();
		}
		return value;
	}
	
	private Object getCurrentCarSettingsValue( String key ) throws NoSuchKeyInSettingsException {
		if( !selectedCar.containsKey( key ) ){
			throw new NoSuchKeyInSettingsException( key );
		}
		return selectedCar.get( key );
	}
	
	/** Gets saved setting value of current car by key */
	public Object getCarValue( String key ) {
		Object value = null;
		try{
			value = getCurrentCarSettingsValue( key );
		} catch ( NoSuchKeyInSettingsException ex ){
			Gdx.app.error( "Exeption while loading car setting", ex.getMessage() );
			Gdx.app.exit();
		}
		return value;
	}
	
	public Integer getIntValue( String key ){
		return new Float( (Float)getValue( key )).intValue();
	}
	
	public Boolean getBoolValue( String key ){
		return (Boolean)getValue( key );
	}
	
	public Float getFloatValue( String key ){
		return (Float)getValue( key );
	}
	
	public Float[] getFloatArrayValue( String key ){
		return (Float[])getValue( key );
	}
	
	public String getStringValue( String key ){
		return (String)getValue( key );
	}
	
	public Integer getIntCarValue( String key ){
		return new Float( (Float)getCarValue( key )).intValue();
	}
	
	public Boolean getBoolCarValue( String key ){
		return (Boolean)getCarValue( key );
	}
	
	public Float getFloatCarValue( String key ){
		return (Float)getCarValue( key );
	}
	
	public Float[] getFloatArrayCarValue( String key ){
		return (Float[])getCarValue( key );
	}
	
	public String getStringCarValue( String key ){
		return (String)getCarValue( key );
	}
	
	/** Sets car of given name to be selected in selectedCar hashMap */
	public void selectCar( int index ) throws NoSuchCarInSettingsException {
		if( carsSettings.size() < index ){
			throw new NoSuchCarInSettingsException( index );
		}
		selectedCar = carsSettings.get( index );
	}
	
	/**
	 * Returns instance of class or creates new
	 * if instance is null
	 * @return this class's instance
	 */
	public static Settings getInstance() {
		if( instance == null ){
			instance = new Settings();
			Gdx.app.log("Settings", "created Settings module");
			instance.loadSettings();
			try{
				instance.selectCar(instance.DEFAULT_CAR_INDEX);
			} catch ( NoSuchCarInSettingsException ex ){
				Gdx.app.error("Exception while loading default car", " "+ex.getMessage());
				Gdx.app.exit();
			}
		}
		return instance;
	}
	
	/**
	*	Method for loading config file
	*	@todo config file implementation/parsing (xml?)
	*/
	private void loadSettings () {
		configFilesLoader = new ConfigFilesLoader();
	}
}
