package com.elchupacabra.elchupapatrol.exceptions;

public class NoSuchKeyInSettingsException extends Exception {

	private static final long serialVersionUID = -4293210590056320439L;
	private String key;
	
	public NoSuchKeyInSettingsException( String key ){
		this.key = key;
	}
	
	public String getMessage(){
		return 	"Given key does not exist in current configuration: "+ this.key +".\n"+
				"it's possible, that config files are either misconfigured or broken.";
	}
}
