package com.elchupacabra.elchupapatrol.exceptions;

public class WrongConfigParameterException extends Exception {

	private static final long serialVersionUID = 427485220453275061L;
	private String configLine;
	private int lineNumber;
	private String filename;
	
	public WrongConfigParameterException( String configLine, int lineNumber, String filename ){
		this.configLine = configLine;
		this.lineNumber = lineNumber;
		this.filename = filename;
	}
	
	public String getMessage(){
		return "Wrong configuration parameter: '" + this.configLine + 
				"'\nin line: " + this.lineNumber +
				"\nin file: " + this.filename;
	}
}
