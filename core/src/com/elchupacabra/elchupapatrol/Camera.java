package com.elchupacabra.elchupapatrol;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
*	@author Łukasz Piotrowski
*
*	Singleton Class
*	
*	Camera class holds main game camera for use with other classes
*	(mainly to extract combined matrices).
*/
public final class Camera {
	/** OrthographicCamera that's used in game as a main camera */
	public OrthographicCamera camera;
	
	private static Camera instance = null;
	
	private Camera() {
		camera = new OrthographicCamera();
		int[] resolution = {new Float( (Float)Settings.getInstance().getValue( "resX" )).intValue(), 
							new Float( (Float)Settings.getInstance().getValue( "resY" )).intValue() };
		camera.setToOrtho(false, resolution[0], resolution[1]);
	}
	
	/**
	*	Returns instance of given class, or creates new
	*	if that instance is nonexistent
	*	@return Class object instance
	*/
	public static Camera getInstance() {
		if( instance == null ){
			instance = new Camera();
			Gdx.app.log("Camera", "created Camera module");
		}
		return instance;
	}
}
