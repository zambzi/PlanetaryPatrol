package com.elchupacabra.elchupapatrol.pawns;

import com.elchupacabra.elchupapatrol.entities.DynamicSpriteEntity;

public class testPawn extends DynamicSpriteEntity {

	public testPawn(float x, float y, float width, float height, String shapeType) {
		super(x, y, width, height, shapeType);
	}
	
	public testPawn(float x, float y, float size, String shapeType) {
		super(x, y, size, shapeType);
	}
	
	public void update() {
		super.update();
	}

}
