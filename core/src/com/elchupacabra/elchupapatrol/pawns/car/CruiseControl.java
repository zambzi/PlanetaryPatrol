package com.elchupacabra.elchupapatrol.pawns.car;

import com.badlogic.gdx.math.Vector2;

public class CruiseControl {
	
	protected CruiseControl () {}
	
	protected void forceConstantSpeed ( Car car ){
		float currentVelocity = car.chassis.getBody().getLinearVelocity().x;
		float deltaVelocity = currentVelocity;
		if( deltaVelocity < 0 ){
			pedalToMetal( car );
		} else if( deltaVelocity > 0 ){
			hitBrakes( car );
		}
	}
	
	private void hitBrakes( Car car ){
		changeTorque( car, 50.0f );
	}
	
	private void pedalToMetal( Car car ){
		changeTorque( car, -50.0f );
	}
	
	private void changeTorque( Car car, float torque ){
		for( Wheel wheel : car.wheels ){
			wheel.getBody().applyTorque( torque, true );
		}
	}
	
}
