package com.elchupacabra.elchupapatrol.pawns.car;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef.JointType;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.elchupacabra.elchupapatrol.Camera;
import com.elchupacabra.elchupapatrol.entities.DynamicSpriteEntity;
import com.elchupacabra.elchupapatrol.entities.EntityUserData;
import com.elchupacabra.elchupapatrol.physics.BulletGenerator;
import com.elchupacabra.elchupapatrol.physics.Physics;
import com.elchupacabra.elchupapatrol.utils.Settings;

public class Turret extends DynamicSpriteEntity {
	
	Joint turretPivot = null;
	Vector2 targetVector = null;
	BulletGenerator firingMechanizm = null;

	public Turret( Chassis chassis ) {
		super( 	0,
				0,
				Settings.getInstance().getFloatCarValue( "turretSizeX" ),
				Settings.getInstance().getFloatCarValue( "turretSizeY" ),
				"box" );
		this.setDensity( Settings.getInstance().getFloatCarValue( "turretDensity" ) );
		this.setRestitution( Settings.getInstance().getFloatCarValue( "turretRestitution" ) );
		this.setTexture("turret", false );
		this.setFixtureUserData( new EntityUserData( EntityUserData.PawnType.TURRET ) );
		this.setTurretPivotJoint( chassis );
		this.removeTurretCollision();
		this.rotateToTarget();
		firingMechanizm = new BulletGenerator( 1300, 20, 200 );
	}
	
	private void removeTurretCollision(){
		Filter filter = new Filter();
		filter.maskBits = 0x0000;
		this.getBody().getFixtureList().get(0).setFilterData( filter );
	}
	
	protected void setTurretPivotJoint( Chassis chassis ){
		Settings settings = Settings.getInstance();
		Vector2 chassisPosition = new Vector2( settings.getFloatCarValue( "turretPosX" ),
				settings.getFloatCarValue( "turretPosY" ));
		Vector2 pivotPosition = new Vector2( settings.getFloatCarValue( "turretPivotX" ),
				settings.getFloatCarValue( "turretPivotY" ));
		RevoluteJointDef jointDef = new RevoluteJointDef();
		jointDef.type = JointType.RevoluteJoint;
		jointDef.initialize( chassis.getBody(), this.getBody(),chassisPosition);
		jointDef.localAnchorA.set( chassisPosition );
		jointDef.localAnchorB.set( pivotPosition );
		this.getBody().setLinearDamping(settings.getFloatCarValue( "turretDamping" ));
		this.turretPivot = Physics.getInstance().world.createJoint( jointDef );
	}
	
	private void rotateToTarget() {
		Settings settings = Settings.getInstance();
		Vector2 target = new Vector2((float)Gdx.input.getX()/settings.getFloatValue( "pixelToMeterModifier" ),
				(float)Gdx.input.getY()/settings.getFloatValue( "pixelToMeterModifier" ) );
		Vector2 pivot = new Vector2( 
				turretPivot.getAnchorB().x , 
				settings.getFloatValue( "resY" )/settings.getFloatValue( "pixelToMeterModifier" )
				- this.turretPivot.getAnchorB().y - settings.getFloatCarValue( "turretPosY" ) );
//		Gdx.app.log("Turret pivot", String.valueOf(pivot.x) +" | " + String.valueOf(pivot.y));
		targetVector = target.sub( pivot );
		float angle = targetVector.angleRad();
		this.getBody().setTransform(this.getBody().getPosition(), -angle);
	}
	
	public void fire( ){
		Settings settings = Settings.getInstance();
		Vector2 barrelPivot = new Vector2( 
				( turretPivot.getAnchorB().x)*settings.getFloatValue( "pixelToMeterModifier" ),
				( turretPivot.getAnchorB().y)*settings.getFloatValue( "pixelToMeterModifier" ));
		Vector2 barrelEnd = new Vector2(
				Settings.getInstance().getFloatCarValue( "turretSizeX" ),0);
		
		barrelEnd.rotate( -targetVector.angle() );
		barrelEnd = barrelPivot.add( barrelEnd );
		firingMechanizm.generateBullet( 
				barrelEnd.x, 
				barrelEnd.y, 
				targetVector );
	}
	
	private void turretControl(){
		if( Gdx.input.isButtonPressed( Input.Buttons.LEFT ) ){
			this.fire();
		}
	}
	
	@Override
	public void update(){
		rotateToTarget();
		firingMechanizm.update();
		super.update();
		this.turretControl();
	}
}
