package com.elchupacabra.elchupapatrol.pawns.car;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.JointDef.JointType;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.WheelJointDef;
import com.elchupacabra.elchupapatrol.physics.Physics;
import com.elchupacabra.elchupapatrol.utils.Settings;

public class Suspension {
	
	private Joint joint = null;
	
	public Suspension ( Chassis chassis, Wheel wheel ) {
		createSuspension( chassis, wheel );	
	}
	
	private WheelJointDef getJointDef ( Chassis chassis, Wheel wheel ) {
		WheelJointDef jointDef = new WheelJointDef();
		jointDef.type = JointType.WheelJoint;
		jointDef.initialize( wheel.getBody(), chassis.getBody(), chassis.getBody().getPosition(), new Vector2( 0, 1 ) );
		jointDef.enableMotor = true;
		jointDef.collideConnected = false;
		jointDef.bodyA = chassis.getBody();
		jointDef.bodyB = wheel.getBody();
		jointDef.dampingRatio = Settings.getInstance().getFloatCarValue( "suspensionDamping" );
		jointDef.frequencyHz = Settings.getInstance().getFloatCarValue( "suspensionFrequency" );
		
		return jointDef;
	}
	
	private void createSuspension( Chassis chassis, Wheel wheel ){
		joint = Physics.getInstance().world.createJoint( getJointDef( chassis, wheel ) );
	}
}
