package com.elchupacabra.elchupapatrol.entities;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.elchupacabra.elchupapatrol.AutoRenderer;
import com.elchupacabra.elchupapatrol.physics.Physics;

/**
 * 
 * @author Łukasz Piotrowski
 * 
 * This is parent class for all Entities
 * and all the classes that derive from them.
 * 
 * Remember to use Entity::Destroy() when removing Entity from
 * world!
 *
 */
public abstract class Entity {
	
	/** Box2D Fixture linking physics engine with Entity */
	protected Fixture fixture = null;
	/** Physics instance reference */
	protected Physics physics;
	
	private int uuid;
	
	public Entity () {
		physics = Physics.getInstance();
		Random random = new Random( System.currentTimeMillis() );
		uuid = random.nextInt();
		AutoRenderer.getInstance().inject( this , AutoRenderer.GAME_LAYER );
	}
	
	/**
	 * Sets fixture's userData. Useful for setting fixture type,
	 * so it can be recognized by collision listener
	 * @param userData EntityUserData instance.
	 */
	public void setFixtureUserData( EntityUserData userData ) {
		if( fixture != null )
			fixture.setUserData( userData );
	}
	
	public Object getFixtureUserData() {
		if( fixture != null ){
			return fixture.getUserData();
		} else {
			return null;
		}
	}
	
	/**
	 * Returns Body from Entity's Fixture
	 * @return Box2D's Body
	 */
	public Body getBody(){
		return fixture.getBody();
	}
	
	/**
	 * Moves fixture to position x,y
	 * @param x position in meters (see Settings::PHYS_PIXEL_METER_MODIFIER)
	 * @param y position in meters (see Settings::PHYS_PIXEL_METER_MODIFIER)
	 */
	public void move ( float x, float y ) {
		fixture.getBody().setTransform( x, y, fixture.getBody().getAngle() );
	};
	
	/**
	 * Moves fixture by x,y meters
	 * @param x position in meters (see Settings::PHYS_PIXEL_METER_MODIFIER)
	 * @param y position in meters (see Settings::PHYS_PIXEL_METER_MODIFIER)
	 */
	public void moveRelative ( float x, float y ) {
		fixture.getBody().setTransform(	x + fixture.getBody().getPosition().x,
										y + fixture.getBody().getPosition().y, 
										fixture.getBody().getAngle() );
	}
	
	/**
	 * Orient fixture in degrees
	 * @param orientation
	 */
	public void orient ( float orientation ){
		float x = fixture.getBody().getPosition().x;
		float y = fixture.getBody().getPosition().y;
		fixture.getBody().setTransform( x, y, ( float )( orientation * Math.PI / 180 ) );
	}
	
	/**
	 * Rotate fixture by given angle in degrees
	 * @param angle
	 */
	public void rotate ( float angle ) {
		float x = fixture.getBody().getPosition().x;
		float y = fixture.getBody().getPosition().y;
		float orientation = fixture.getBody().getAngle() + (float)( angle * Math.PI / 180 );
		fixture.getBody().setTransform(x, y, orientation );
	}
	
	/**
	 * Destroys Fixture within Entity.
	 * ALWAYS use this method when getting rid of Entity,
	 * otherwise the fixture will still exist within box2D world.
	 */
	public void destroy() {
		AutoRenderer.getInstance().removeFromRender( this );
		AutoRenderer.getInstance().removeFromUpdate( this );
		fixture.getBody().destroyFixture(fixture);
	}
	
	public void update(){}
	public void render( PolygonSpriteBatch polyBatch ){}
	public void render(SpriteBatch batch){}
}
