package com.elchupacabra.elchupapatrol.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.elchupacabra.elchupapatrol.AssetsManager;
import com.elchupacabra.elchupapatrol.exceptions.NoAssetWithGivenNameException;
import com.elchupacabra.elchupapatrol.physics.Physics;

/**
 * 
 * @author Łukasz Piotrowski
 * 
 * This is parent class for DynamicEntity and StaticEntity
 * and all the classes that derive from them.
 * This class specifies Entities that mix box2D fixtures
 * and libGdx Sprites, to create physics-enabled pawns
 * in game.
 * 
 * Remember to use Entity::Destroy() when removing Entity from
 * world!
 *
 */
public abstract class SpriteEntity extends Entity {
	
	/** Sprite, as a visual instance of Entity */
	protected Sprite sprite;
	
	public SpriteEntity () {
		physics = Physics.getInstance();
	}
	
	/**
	 * This method allows to set Sprite and all of it's settings
	 * @param x position in world (in pixels)
	 * @param y position in world (in pixels)
	 * @param width width of sprite (in pixels)
	 * @param height height of sprite (in pixels)
	 */
	protected void setSprite ( float x, float y, float width, float height ) {
		sprite = new Sprite( new  Texture( (int)width, (int)height, Format.RGB888 ) );
		sprite.setBounds( x, y, width*2, height*2 );
	}
	
	/**
	 * This method allows to set Texture to Sprite, via AssetsManager class
	 * @param textureName name of a texture loaded via AssetsManager
	 * @param resizeToProportion TODO
	 */
	public void setTexture ( String textureName, boolean resizeToProportion ) {
		try {
			sprite.setRegion( AssetsManager.getInstance().getTexture( textureName ) );
		} catch (NoAssetWithGivenNameException e) {
			e.getMessage();
			Gdx.app.exit();
		}
//		if( resizeToProportion ) {
//			float width = AssetsManager.getInstance().getTexture( textureName ).getWidth(); 
//			float height = AssetsManager.getInstance().getTexture( textureName ).getHeight();
//			height = height * sprite.getWidth() / width;
//			width = sprite.getWidth();
//		}
	}
	
	/**
	 * Sets tint to Sprite
	 * @param red in range [0..1]
	 * @param green in range [0..1]
	 * @param blue in range [0..1]
	 * @param alpha in range [0..1]
	 */
	public void setColor ( float red, float green, float blue, float alpha ) {
		sprite.setColor( red, green, blue, alpha );
	}
	
	/**
	 * This method updates Sprite transformations to match Fixture.
	 */
	@Override
	public void update () {
		float x = fixture.getBody().getPosition().x;
		float y = fixture.getBody().getPosition().y;
		
		float[] origin = {sprite.getWidth()/2, sprite.getHeight()/2};
		float[] pos = physics.getPixelsFromMeters(x, y);
		sprite.setOrigin( origin[0], origin[1]  );
		sprite.setRotation( ( float )( fixture.getBody().getAngle() * 180 / Math.PI ) );
		sprite.setPosition( pos[0] - origin[0], pos[1] - origin[1] );
	};
	
	public Vector2 getPosition (){
		return new Vector2(
				sprite.getOriginX(),
				sprite.getOriginY() );
	}
	
	public Vector2 getDimensions () {
		return new Vector2( 
				sprite.getWidth() * sprite.getScaleX(),
				sprite.getHeight() * sprite.getScaleY() );
	}
	
	/**
	 * This method executes drawing instruction on given batch
	 * @param batch SpriteBatch on which to draw 
	 * (needs to be enclosed with begin() and end()!)
	 */
	@Override
	public void render ( SpriteBatch batch ) {
		sprite.draw(batch);
	}
	
}
