package com.elchupacabra.elchupapatrol.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

/**
 * 
 * @author Łukasz Piotrowski
 *
 * Static Entity allows to create sprites with static fixtures.
 * @todo it might be a good idea to turn this into kinematic entity
 */

public abstract class StaticSpriteEntity extends SpriteEntity{
	
	/**
	 * Creates DynamicEntity with specified dimensions
	 * @param x position in world (in pixels)
	 * @param y position in world (in pixels)
	 * @param width width of Entity (in pixels)
	 * @param height height of Entity (in pixels)
	 */
	public StaticSpriteEntity ( float x, float y, float width, float height ) {
		super();
		setFixture ( x, y, width, height );
		setSprite( x, y, width, height );
	}
	
	/**
	 * Creates DynamicEntity with specified dimensions
	 * @param x position in world (in pixels)
	 * @param y position in world (in pixels)
	 * @param size size of Entity (in pixels)
	 */
	public StaticSpriteEntity ( float x, float y, float size ) {
		super();
		setFixture ( x, y, size, size );
		setSprite( x, y, size, size );
	}
	
	/**
	 * Creates fixture with Static Body Type.
	 * @param x position in world (in pixels)
	 * @param y position in world (in pixels)
	 * @param width width of Fixture (in pixels)
	 * @param height height of Fixture (in pixels)
	 */
	protected void setFixture ( float x, float y, float width, float height ) {
		float[] pos = physics.getMetersFromPixels( x, y );
		float[] size = physics.getMetersFromPixels( width, height );
		
		BodyDef bodyDef = new BodyDef();
		bodyDef.position.set( new Vector2( pos[0], pos[1] ) );
		
		Body body = physics.world.createBody( bodyDef );
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox( size[0], size[1] );
		
		fixture = body.createFixture( shape, 0.0f );
		shape.dispose();
	}
}