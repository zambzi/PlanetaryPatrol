package com.elchupacabra.elchupapatrol.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.elchupacabra.elchupapatrol.ElcPatrolMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
	    config.title = "Elchupa Patrol";
	    config.width = 1280;
	    config.height = 768;
//	    config.fullscreen = true;
		new LwjglApplication(new ElcPatrolMain(), config);
	}
}
